// Ex1

const divCheck = (num) => {
    if (num % 3 === 0 && num % 5 === 0) {
      console.log(`${num} is divisible by 3 and 5`);
    } else {
      console.log(`${num} is not divisible by 3 and 5`);
    }
  };
  
  divCheck(30);
  
  // Ex2
  
  let num = 10;
  if (num % 2 === 0) {
    console.log(`${num} is even number`);
  } else {
    console.log(`${num} is odd number`);
  }
  
  // Ex3
  
  let array = [1, 2, 5, 9, 20, 10];
  console.log(array.sort((a, b) => a - b));
  
  // Ex4
  function uniqueArray(arg) {
    let arr = [];
    arg.map((item) => {
      for (let [key, value] of Object.entries(item)) {
        arr.push(...value);
      }
    });
    let newArr = arr.filter((val, pos) => {
      return arr.indexOf(val) === pos;
    });
    return newArr;
  }
  
  uniqueArray([
    {
      test: ["a", "b", "c", "d"],
    },
    {
      test: ["a", "b", "c"],
    },
    {
      test: ["a", "d"],
    },
    {
      test: ["a", "b", "k", "e", "e"],
    },
  ]);
  
  // ex5
  
  let obj1 = {
    name: "Samar",
    surname: "Badriddinov",
    age: 25,
  };
  let obj2 = {
    name: "Samar",
    surname: "Badriddinov",
    age: 25,
  };
  
  if (JSON.stringify(obj1) === JSON.stringify(obj2)) {
    console.log(`Same objects`);
  } else {
    console.log(`Not the same objects`);
  }
  